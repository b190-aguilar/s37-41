const jwt = require("jsonwebtoken");
// used in algorith for encrypting our data which makes it difficult to decode the information without the defined secret
const secret = "qwerty";

/*
	JSONWEBTOKEN
		- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of the server
		- information is kept secure through the use of the secret code
		- only the system that knows the secret code can decode the encrypted information
*/

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// generate JSON web token using jwt's sign method
	// generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
};


// Token verification
module.exports.verify = (req, res, next) =>	{
	// the token is retrieved from the request header
	// in Postman - Authorization - Bearer token
	let token = req.headers.authorization

	if (typeof token !== "undefined"){
		console.log(token);
		// the token sent is a type of "Bearer" which when received, contains the word "Bearer" as a prefix to the string
		token = token.slice(7, token.length);
		// verify() - validates the token decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			// should the jwt be invalid, error, then return "failed"
			if(err){
				return res.send({auth: "failed"});
			}
			else{
				// allows the application to proceed with the next middleware function/ callback function in the route
				next();
			};
		});
	}
	else{
		return res.send({auth:"failed"});
	};
};


// Token decryption
module.exports.decode = (token) =>{
	if (typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err,data)=>{
			if(err){
				return null;
			}
			else{
				// "decode" method is used to obtain the information from the JWT
				// the "token" argument serves as the one to be decoded
				// {complete:true} - option that allows us to return additional information from the JWT
				// .payload property - conatins the information provided in the "createAccessToken" method defined above (id, email, isAdmin)
				return jwt.decode(token, {complete: true}).payload;
			};
		});
	}
	else{
		// if the token does not exist.
		return null;
	};
};


