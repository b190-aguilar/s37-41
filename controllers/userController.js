const User = require("../models/User.js");
const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

/*
BUSINESS Logic
	- use mongoose method "find" to find duplicate
	- use .then to send a response 
		- "email already exists"


*/
module.exports.checkEmailExists = (reqBody) =>{
	return User.find({email: reqBody.email}).then(result =>{
		// if there is an existing email
		if(result.length > 0){
			return true;
		}
		// if there is no duplicate
		else{
			return false;
		}
	});
};

// User registration
/*
	1. create a new User object using the mongoose model and the information from the request body.
	2. make sure that the password is encrypted.
	3. save the object to the database.
*/
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// hashSync - bcrypt method for encrypting the password of the user once they have successfully registered in our database.
		/*
			first parameter - value to which the encryption will be done (password coming from the request body)
			2nd parameter (10) - dictates how many "salt" rounds are to be given to encrypt the value
		*/
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) =>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
}


// User Login
/*
	1. check the db if the user email exists
	2. compare the password provided in the request body with the password stored in the database
	3. generate/return a JSON web token if the user has successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// if the user does not exist
		if(result === null){
			return false;
		}
		// if the user.email exists in the database
		else{
			// compareSync = a bcrypt method that decodes the encrypted password from the database and compares it to the password received from the request body.
			// it's a good practice the if the value returned by a method is boolean, the var name should be 'is'+'varName'
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return false;
			};
		};
	});
};


// S33 Activity


module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.userId).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
			result.password = "";
			return result;	
		};
	});
};



// enrol a user
/*
	1. to find the document in the db using the userId
	2. add the course ID to the user's enrollments array
	3. update the document in the db
*/
// async await will be used in enrolling since we have two documents to be updated in our database: user document and course document
module.exports.enroll = async (data) =>{
	// adding the courseId in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});

		// returns boolean depending if the updating of the document is successful (true) or not (false)
		return user.save().then((user,error) =>{
			if(error){
				return false;
			}
			else{
				console.log(`User: ${user}`);
				return true;
			};
		});
	});
/*
	try to update the enrollees array in the course documents using the codes above as your guide 
*/
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// adding of userId in the enrollees array
		course.enrollees.push({userId: data.userId});

		// saves the updated course information in the database
		return course.save().then((course,error) =>{
			if(error){
				return false;
			}
			else{
				console.log(`Course: ${course.id}`);
				return true;
			};
		})
	});


	// create a condition that will check if the user and course documents have been updated
	if(isUserUpdated && isCourseUpdated){
		// user enrolment successful
		return true;
	}
	else{
		// one or both is false = user enrolment failed
		return false;
	}
};
