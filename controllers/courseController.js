const Course = require("../models/Course.js");
const User = require("../models/User.js");

// create new Course

/*
BUSINESS LOGIC
	create a new Course object using the mongoose model and the information from the request body
	save the new Course object to the database
*/
module.exports.addCourse = (user,reqBody) =>{
	console.log(user);
	let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		});
	
		return newCourse.save().then((course, error) =>{
			if (error) {
				return false;
			}else{
				return course;
			}
		});
};



// get all courses
module.exports.getAllCourses = () =>{
	return Course.find({}).then(result =>{
		return result;
	});
};


// get all active courses
/*
	retrieve all course in the database with isActive = true
	return the result
*/
module.exports.getAllActive = () =>{
	return Course.find({isActive: true}).then(result =>{
		return result;
	});
};


// get specific course
/*
	retrieve the course that matches the courseId provided in the url
	return the result.
*/
module.exports.getCourse = (reqBody) =>{
	console.log(reqBody);
	return Course.findById(reqBody.courseId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else {
			return result;	
		};
	});
};


// Update a course
/*
	MINIACTIVITY
		1. create a variable "updateCourse" which will contain the information from the requestBody
		2. find and update the course using the courseId found in the request params
		3. return false if there are errors, true if update successful
*/


module.exports.updateCourse = (reqParams,reqBody) =>{
	console.log(reqParams);
	console.log(reqBody);

	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	// findByIdAndUpdate - its purpose is to find a specific id in the database using the 1st parameter, and update using the 2nd parameter
	/*
		.findByIdAndUpdate(documentId, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse ).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
			return {update: "successful"};	
		};
	});
};


// S40 ACTIVITY
// Archive a course
/*
	1. create an object "updateActiveField" and store isActive: false inside
	2. Find and update the course using the courseId found in the request params and the variable "updateActiveField" 
	3. return false if there are errors, true if the updating is successful
*/

module.exports.archiveCourse = (reqParams) => {
	console.log(reqParams);
	let updateActiveField = {
		isActive : false
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
			return {archive: "successful"};	
		};
	})
};