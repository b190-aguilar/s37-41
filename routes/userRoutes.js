const express = require("express");
const auth = require("../auth.js")
const router = express.Router();

const userController = require("../controllers/userController.js");

// route for checking if user's email already exists in database
// since we will pass a "body"  from the request object, post method will be used as HTTP method even if the goal is to just check the database if there is a user email saved in it.

router.post("/checkEmail", (req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// route for user registration
/*
	pass the request body that has the contents of the user credentials to be saved in our database
		call the fxn "registerUser" from our userController
*/

router.post("/register", (req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});



// route for user authentication
router.post("/login", (req,res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// ACTIVITY S33

// route for user details
router.get("/details", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});


// route for user enrolment
router.post("/enroll", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});



module.exports = router;