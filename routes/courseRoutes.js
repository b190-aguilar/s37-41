const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");


// route for creating a course
router.post("/registercourse", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	if(userData.isAdmin === false){
		res.send({auth: "Unauthorised user"});
	}
	else{
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	};
	
});

// route for getting all courses
router.get('/all',(req,res)=>{
courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


// route for getting all Active courses
router.get('/allactive',(req,res)=>{
courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});



// route for getting a specific course using id params
router.get('/:courseId',(req,res)=>{
courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// route for updating course
router.put('/:courseId', (req,res)=>{
	courseController.updateCourse(req.params,req.body).then(resultFromController => res.send(resultFromController));
});


// route for archiving course
router.put('/:courseId/archive', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization).isAdmin;
	if(userData){
		courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
	} 
	else{
		res.send({auth: "Unauthorised user"});
	}
});

module.exports = router;